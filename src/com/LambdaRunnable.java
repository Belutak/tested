package com;

public class LambdaRunnable {

    public static void main(String[] args) {
        Thread myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Inside RUnnable");
            }
        });

        myThread.run();


        Thread myLambdaThread = new Thread(() ->
                System.out.println("Inside Runnable"));

        myLambdaThread.run();
    }


}
