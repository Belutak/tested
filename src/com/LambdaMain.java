package com;

public class LambdaMain {
    public static void main(String[] args) {

        //LAMBDA tests
        FuncInter result = (int a, int b) -> a+b;
        System.out.println(result.calculate(2, 3));

        SayHello sayHello = new SayHelloImpl();
        //sayHello.useWords();
        SayHello lambdaWay = () -> System.out.println("Hello world!");
        //anonymous inner class
        SayHello innerClass = new SayHello() {
            @Override
            public void useWords() {
                System.out.println("Hello world in inner class!");
            }
        };

        Ello sayEllo = new Ello();
        System.out.println("new");
        sayEllo.hello(innerClass);
        sayEllo.hello(lambdaWay);

    }

    interface FuncInter {
        int calculate(int a, int b);
    }

    public static class Ello {
        public void hello(SayHello helloThere) {
            helloThere.useWords();
        }
    }
}
