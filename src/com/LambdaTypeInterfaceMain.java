package com;

public class LambdaTypeInterfaceMain {
    public static void main(String[] args) {
        //StringLengthLambda myLambda = (String s) -> s.length();
//        StringLengthLambda myLambda = s -> s.length();
//        System.out.println(myLambda.getLength("Hey Lambda"));
        pringLambda(s -> s.length());

    }

    public static void pringLambda(StringLengthLambda l) {
        System.out.println(l.getLength("Hello lambda"));
    }

    interface StringLengthLambda {
        int getLength(String s);
    }
}
