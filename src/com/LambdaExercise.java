package com;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class LambdaExercise {

    public static void main(String[] args) {

        List<Person> people = Arrays.asList(
                new Person("Charles", "Dickens", 60),
                new Person("Lewis", "Carrol", 42),
                new Person("Thomas", "Carlyle", 51),
                new Person("Charlotte", "Bronte", 45),
                new Person("Matthew", "Arnold", 39)

        );


        //Java 7
//        Collections.sort(people, new Comparator<Person>() {
//            @Override
//            public int compare(Person o1, Person o2) {
//                return o1.getLastName().compareTo(o2.getLastName());
//            }
//        });
        //Java 8 lambda ex
        Collections.sort(people, (p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));

        System.out.println("\nPrinting all peeps");
        //printAll(people);
        performConditionally(people, p -> true, p -> System.out.println(p));


        //Java 7
//        System.out.println("All peeps with last name starting with C");
//        printConditionally(people, new Condition() {
//            @Override
//            public boolean test(Person p) {
//                return p.getLastName().startsWith("C");
//            }
//        });
        //Java 8 lambda ex
        System.out.println("\nAll peeps with last name starting with C");
        performConditionally(people, p -> p.getLastName().startsWith("C"), p -> System.out.println(p));

        System.out.println("\nAll peeps with first name starting with C");
        performConditionally(people, p -> p.getFirstName().startsWith("C"), p -> System.out.println(p.getFirstName()));

    }
    //use our interface Condition
//    private static void printConditionally(List<Person> people, Condition condition) {
//        for (Person p : people) {
//            if (condition.test(p)) {
//                System.out.println(p);
//            }
//        }
//    }
    //use Java functional interface Predicate instead of our Condition
//    private static void printConditionally(List<Person> people, Predicate<Person> predicate) {
//        for (Person p : people) {
//            if (predicate.test(p)) {
//                System.out.println(p);
//            }
//        }
//    }

    private static void performConditionally(
            List<Person> people, Predicate<Person> predicate,
            Consumer<Person> consumer) {

        for (Person p : people) {
            if (predicate.test(p)) {
                consumer.accept(p);
            }
        }
    }

//    private static void printAll(List<Person> people) {
//        for (Person p : people) {
//            System.out.println(p);
//        }
//    }

}

//interface Condition {
//    boolean test(Person p);
//}